package cn.turing.firecontrol.server.controller;


import cn.turing.firecontrol.server.handler.device.DeviceEventHandlerComposite;
import cn.turing.firecontrol.server.vo.DeviceSensorMessage;
import com.alibaba.fastjson.JSONObject;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.concurrent.ExecutorService;

/**
 *以防万一，kafka ，或者rabbitmq服务挂掉，暴露此接口
 */
@Controller
@RequestMapping("/dataanalysis")
@Slf4j
public class ReceiveDeviceController {

    @Autowired
    DeviceEventHandlerComposite deviceEventHandlerComposite;

    @Autowired
	ExecutorService executorService;

    @ResponseBody
    @RequestMapping("/device")
    public String receiveMessageByDevice(@RequestBody String data){
    //{"deviceType":"TZJ_SS","iccid":"111111","data":"5050090201600559011C0348E211","id":"004a770124042749","time":"20200523142439","version":"1.0.0"}
        log.info("接收推送的 message:{}", data);
        executorService.execute(()->{
			JSONObject jsonObject= JSONObject.parseObject(data);
			DeviceSensorMessage deviceSensorMessage= JSONObject.toJavaObject(jsonObject,DeviceSensorMessage.class);

			deviceEventHandlerComposite.prcess(deviceSensorMessage);

		});

        return "200";
    }

}
